<?php

require __DIR__ . '/Product.php';

$product = new Product([
	'name' => 'Go Run',
	'price' => 60,
	'discountprice' => 60
]);

var_dump($product);
// echo $product->price;
// echo $product->name;

var_dump($product->hasDiscount());
var_dump($product->getDiscount());


$product = new Product([
	'name' => 'BOBS',
	'price' => 100,
	'discountprice' => 85
]);
var_dump($product);
// echo $product->price;
// echo $product->name;
var_dump($product->hasDiscount());
var_dump($product->getDiscount());