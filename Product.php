<?php 

class Product {
	public $price;
	public $name;
	public $discountprice;

	public function __construct($productData)
	{
		// $this->name = $name;
		// $this->price = $price;
		// $this->discountprice = $discountprice;
		 
		$this->name = $productData['name'];
		$this->price = $productData['price'];
		$this->discountprice = $productData['discountprice'];
	}

	public function hasDiscount()
	{
		if ($this->discountprice < $this->price) {
			return true;
		}

		return false;
	}

	public function getDiscount()
	{
		return 100 - ($this->discountprice / $this->price) * 100;
	}

  // A getter method for the price property
	public function getPrice()
	{
		return $this->price;
	}

	// A setter method for the price property
	public function setPrice($price)
	{
		$this->price = $price;
	}
}